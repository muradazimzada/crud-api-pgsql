import dto.StudentDto;
import entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repository.StudentRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/students")
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    // Create a new student
    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto studentDto) {
        Student student = new Student();
        student.setName(studentDto.getName());
        student.setAge(studentDto.getAge());
        student.setDepartment(studentDto.getDepartment());
        Student savedStudent = studentRepository.save(student);

        // Convert the saved student entity back to a DTO and return it
        return new StudentDto((savedStudent.getId()), savedStudent.getName(), savedStudent.getAge(), savedStudent.getDepartment());
    }

    // Get all students
    @GetMapping
    public List<StudentDto> getAllStudents() {
        List<Student> students = studentRepository.findAll();
        return students.stream()
                .map(student -> new StudentDto(student.getId(), student.getName(), student.getAge(), student.getDepartment()))
                .collect(Collectors.toList());
    }

    // Get student by ID
    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Integer id) {
        Student student = studentRepository.findById(id).orElse(null);
        if (student != null) {
            return new StudentDto(student.getId(), student.getName(), student.getAge(), student.getDepartment());
        }
        return null;
    }

    // Update student by ID
    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Integer id, @RequestBody StudentDto studentDto) {
        Student student = studentRepository.findById(id).orElse(null);
        if (student != null) {
            student.setName(studentDto.getName());
            student.setAge(studentDto.getAge());
            student.setDepartment(studentDto.getDepartment());
            Student updatedStudent = studentRepository.save(student);
            return new StudentDto((updatedStudent.getId()), updatedStudent.getName(), updatedStudent.getAge(), updatedStudent.getDepartment());
        }
        return null;
    }

    // Delete student by ID
    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Integer id) {
        studentRepository.deleteById(id);
    }
}
